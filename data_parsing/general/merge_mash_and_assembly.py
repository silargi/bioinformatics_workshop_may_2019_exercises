
import pandas as pd
import re
mash_refseq_details = pd.read_csv("all_complete_bacteria_refseq.k21s1000.info.txt", sep="\t")
assembly_refseq_details = pd.read_csv("assembly_summary.txt", sep="\t", header=1, low_memory = False)


# need to use only accessions without versions
# add new column GCF_accession_without_version in mash_ref_seq_details
mash_refseq_details['GCF_accession_without_version'] = mash_refseq_details['GCF_accession'].str.replace(
    r'\.\d+$',
    ''
)
assembly_refseq_details['GCF_accession_without_version'] = assembly_refseq_details['# assembly_accession'].str.replace(
    r'\.\d+$',
    ''
)

# reorder columns 
mash_refseq_details = mash_refseq_details[['accession', 'GCF_accession', 'GCF_accession_without_version', 'filename', 'length', 'num_contigs', 'info' ]]

# merge the files
merged = mash_refseq_details.merge(
    assembly_refseq_details[['GCF_accession_without_version', 'bioproject', 'biosample', 'refseq_category', 'taxid', 'species_taxid', 'organism_name', 'infraspecific_name', 'assembly_level', 'asm_name', 'submitter']],
    on = ['GCF_accession_without_version'],
    how = 'left'
)

# add excluded for those where there are no matches as the most likely cause
merged.loc[merged['organism_name'].isnull(),'organism_name'] = 'excluded'
# rename organism_name to full_organism_name
merged = merged.rename(columns = {'organism_name': 'full_organism_name'})
merged['organism_name'] = merged['full_organism_name'].str.split(' ').str[1:3].str.join(' ')
merged = merged[['accession', 'GCF_accession', 'GCF_accession_without_version', 'filename', 'length', 'num_contigs', 'info', 'organism_name', 'full_organism_name', 'taxid', 'species_taxid', 'bioproject', 'biosample','refseq_category', 'infraspecific_name', 'assembly_level', 'asm_name', 'submitter']]


# write out file
merged.to_csv("all_complete_bacteria_refseq.k21s1000.species.tsv", sep = "\t", index = False)




