# Parallel in bash

### sequential
```bash
for f in GCF_*.msh; do
    mash dist $f refseq.genomes.k21s1000.msh | sort -grk3 | tail -1 | awk '{print $2}' >> best_matches.txt;
done
```

### parallel
```bash
parallel --eta -j 4 \
    "mash dist {} refseq.genomes.k21s1000.msh | sort -grk3 | tail -1 > best_matches.txt" \
::: GCF_*.msh
```

# Parallel in python
```python
from multiprocessing import Pool
import subprocess

def run_mash_sketch(fasta_file, number_of_hashes):
    output_prefix = '.'.join(os.path.basename(fasta_file).split('.')[:-1]
    subprocess.run(['mash', 'sketch', '-s', number_of_hashes, '-o', output_prefix, fasta_file])


pool=Pool(processes=4)
fasta_files = glob.glob("/data/assemblies/*.fas"))

number_of_hashes = 10000
sketch_files = pool.starmap(run_mash_sketch, [(fasta_file, number_of_hashes) for fasta_file in fasta_files])
```