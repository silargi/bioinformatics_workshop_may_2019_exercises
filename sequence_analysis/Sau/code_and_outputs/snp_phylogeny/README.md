# S.aureus ST22 SNP analysis
## Determining reference
 * Installed bactinspectorMax 
```
pip uninstall bactinspectorMax
pip install https://gitlab.com/antunderwood/bactinspector/raw/max/bactinspector/dist/BactInspectorMax-0.0.1.tar.gz --user
```

 * Got closest matches by `bactinspectorMax closest_match -fq '*R1.fasta.gz'`
 * The output can be found [closest_matches_2019-05-22.tsv](closest_matches_2019-05-22.tsv)
 * The best match was NZ_CP026064.1. Downloaded using wget and the link in the tsv file. Uploaded this to Pathogenwatch and confirmed this was ST22

## SNP Phyogeny generation
 * Ran the analysis with the script [run_snp_phylogeny.sh](run_snp_phylogeny.sh)
 * The final tree output can be found [aligned_pseudogenome.gubbins.variants_only.contree](aligned_pseudogenome.gubbins.variants_only.contree)
 * Mid point rooted and ordered (descending nodes)[aligned_pseudogenome.gubbins.variants_only.midpoint_rooted.tre](aligned_pseudogenome.gubbins.variants_only.midpoint_rooted.tre)
 * Used the CSV from roary for the microreact metadata but put the id column first and addded the reference accession as a new row
 [miocroreact link](https://microreact.org/project/rOL5pwv-L) 