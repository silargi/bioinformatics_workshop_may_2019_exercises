#!/usr/bin/env bash
#BSUB -o /lustre/scratch118/infgen/team212/au3/saureus_st22/cluster_logs/%J.o
#BSUB -e /lustre/scratch118/infgen/team212/au3/saureus_st22/cluster_logs/%J.e
#BSUB -R "select[mem>1024] rusage[mem=1024]"
#BSUB -M 1024
#BSUB -q long

nexflow_workflows_dir='/nfs/users/nfs_a/au3/nextflow_workflows'
data_dir='/lustre/scratch118/infgen/team212/au3/saureus_st22'

/nfs/users/nfs_a/au3/bin/nextflow run \
${nexflow_workflows_dir}/assembly/assembly.nf \
--adapter_file ${nexflow_workflows_dir}/assembly/adapters.fas \
--qc_conditions ${nexflow_workflows_dir}/assembly/qc_conditions_nextera.yml.original \
--input_dir  ${data_dir}/fastqs \
--fastq_pattern '*{R,_}{1,2}.fastq.gz' \
--output_dir ${data_dir}/assembly_output \
--minimum_scaffold_length 1000 \
--depth_cutoff 100 \
--confindr_db_path /lustre/scratch118/infgen/team212/au3/singularity/confindr_database \
-profile sanger \
-resume